# Introducción
Esta utilidad permite detectar Data Skew en una ORG. 

No conozco que Salesforce ofrezca, ninguna utilidad para la detección de Data Skew, aunque su precoz detección puede ahorrarnos muchos dolores de cabeza. 
Por ello, he desarrollado esta utilidad en APEX que te va a permitir, analizar tu ORG, e informar a tus compañeros, administradores y clientes de si existe Data Skew.

Lo he desarrollado en Apex, bajo los siguientes criterios:

*  Eecutable en ORGs tanto pequeñas, como en ORGs de gran tamaño
*  Utilizando mecanismos de Batch, para no afectar nunca el online y asegurar que el volumen de la ORG no sea un inconveniente
*  Mejor rendimiento posible
*  Código lo más comprensible posible (write code for humans) dentro de mis conocimientos y PMD-Friendly
*  No realizar ninguna modificación en los objetos existentes de la ORG
*  Crear los menos posibles (solo he creado 3 objetos como veremos)
   
Esta utilidad se describe en las siguientes entradas de Blog:
  
  * En qué consisten las anomalías en datos de Data Skew: [https://forcegraells.com/2018/07/19/skew-data/]
  * Explicación sobre el código y los objetos utilizados: [http://forcegraells.com/2018/08/27/data-skew-detector-apex/]d
    

## Consejos de utilización para ORGs con grandes Volúmenes de datos
### Creación de índices Custom
En ORGs con grandes volúmenes de datos, las queries realizadas por la aplicación, en algunos campos podría ser que no tuvieran un valor de "selectivity" suficiente, por lo que la query tarde más de lo esperado. En tal caso, puedes crear índices custom temporales sobre esos campos, solo en los objetos más voluminosos, y obtendrás una mejora del rendimiento, ya que el optimizador de SOQL obtendrás valores de selectivity aceptables (Para más información sobre este tema consulta este enlace y descarga la Query&Search Cheat Sheet. [https://developer.salesforce.com/blogs/engineering/2013/07/maximizing-the-performance-of-force-com-soql-reports-and-list-views.html]).

Posteriormente puedes eliminar esos índices, y volver al estado original.

### Utilización de Query Plan mediante API REST para obtener el Count de las tablas de datos
La aplicación utiliza Database.countQuery() para obtener el número de registros de las tablas de datos. En ORGs con grandes volúmenes de datos, estas queries pueden consumir algo más de pocos segundos. Alternativamente a este procedimiento, existe la posibilidad de consumir la API REST desde Apex consultando el Query Plan. El resultado de Query Plan retorna un parámetro que es *sobjectCardinality* que contiene un valor aproximado del número de registros del objeto.
Por tanto, podríamos considerar la sustitución de las SOQL de Count por la invocación a /services/data/vXX.X/query/?explain=QUERY para obtener ese valor.

Más información aquí [https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_query_explain.htm]