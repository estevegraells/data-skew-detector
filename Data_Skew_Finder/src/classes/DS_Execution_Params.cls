/**
 * Class that reads execution parameters from Metadata Custom Type
 *
 * @author Esteve Graells
 * @date July 2018
 *
 * @description Read execution parameters and share the values using static variables
 * Get full explanation on https://forcegraells.com/2018/07/19/skew-data/
 *
*/

global class DS_Execution_Params {

    global static Boolean EXECUTION_PARAMETERS_OK = false;

    global static Boolean RELOAD_COUNT_ROW_RELATIONS_OBJECT = true;

    global static Integer SKEW_LIMIT = 10000;

    global static Boolean INCLUDE_CUSTOM_OBJECTS = false;

    global static List<String> STANDARD_OBJECTS_TO_CHECK_LIST = new List<String>();

    global static List<String> CUSTOM_OBJECT_TO_EXCLUDE_LIST = new List<String>();

    global static List<String> DONT_CHECK_FIELDS_LIST = new List<String>();

    //Read values and allow them to be access using static variables
    static {

        Integer paramsExecutionActiveRecords = Database.countQuery(
                'SELECT count() ' +
                'FROM Skew_Data_Setting__mdt ' +
                'WHERE  IsActive__c=true');

        //I've been dreaming on using switch/when from day one, so here it goes
        switch on paramsExecutionActiveRecords {

            when 0 {
                Logger.insertLogEntry('Not active metadata parameters configuration - execution halted');
                return;
            }
            when 1 {
                EXECUTION_PARAMETERS_OK = true;
            }
            when else {
                Logger.insertLogEntry('Several Active metadata parameters configuration found, check this log to see which one was actually picked up');
                EXECUTION_PARAMETERS_OK =true;
            }
        }

        if (EXECUTION_PARAMETERS_OK) {

            RELOAD_COUNT_ROW_RELATIONS_OBJECT = ([
                    SELECT Reload_Count_Row_and_Relations_Table__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Reload_Count_Row_and_Relations_Table__c);

            SKEW_LIMIT = Integer.valueOf([
                    SELECT Data_Skew_Limit__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Data_Skew_Limit__c);

            SKEW_LIMIT = (SKEW_LIMIT == null ? 10000 : SKEW_LIMIT);

            INCLUDE_CUSTOM_OBJECTS = [
                    SELECT Check_Custom_Objects_for_Data_Skew__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Check_Custom_Objects_for_Data_Skew__c;

            INCLUDE_CUSTOM_OBJECTS = (INCLUDE_CUSTOM_OBJECTS == null ? true : INCLUDE_CUSTOM_OBJECTS);

            String Std_Obj_To_Check =
                    [SELECT Standard_Object_To_Check_List__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Standard_Object_To_Check_List__c;
            if (Std_Obj_To_Check != null) STANDARD_OBJECTS_TO_CHECK_LIST = Std_Obj_To_Check.trim().split('\\;');

            String Cust_Obj_To_Avoid = [
                    SELECT Custom_Object_List_To_avoid_to_check__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Custom_Object_List_To_avoid_to_check__c;
            if (Cust_Obj_To_Avoid != null) CUSTOM_OBJECT_TO_EXCLUDE_LIST = Cust_Obj_To_Avoid.trim().split('\\;');


            String Fields_To_Avoid = [
                    SELECT Field_List_Not_to_be_checked__c
                    FROM Skew_Data_Setting__mdt
                    WHERE IsActive__c = TRUE
                    LIMIT 1].Field_List_Not_to_be_checked__c;
            if (Fields_To_Avoid != null) DONT_CHECK_FIELDS_LIST = Fields_To_Avoid.trim().split('\\;');
        }
    }
}