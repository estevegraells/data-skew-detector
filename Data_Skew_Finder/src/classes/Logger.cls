/**
 * Created by estev on 31/01/2018.
 */

public class Logger {

    /*
    * Insert an Event in the Log Object
    * */


    public static void insertLogEntry(String Description) {
        Process_Log__c newLog = new Process_Log__c(Description__c = Description);

        insert newLog;
    }

}