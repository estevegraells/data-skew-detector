/**
 * As data in our objects can be skewed https://developer.salesforce.com/blogs/engineering/2012/04/avoid-account-data-skew-for-peak-performance.html
 * this class sets up which standard and custom objects must be checked to find Data skew, not only Account, but all you need.
 *
 * @author Esteve Graells
 * @date July 2018
 *
 * @description Main class for Data Skew Finder - find data and ownership skew not only in Account but in all object you need across your org
 * Get full explanation on https://forcegraells.com/2018/07/19/skew-data/
 *
*/

global with sharing class DS_Finder implements Schedulable {

    //Map that contains collected objects to check for Data Skew
    public static List<Data_Skew_Object__c> objectsToCheck = new List<Data_Skew_Object__c>();

    public DS_Finder() {

        Logger.insertLogEntry('Start: ' + Datetime.newInstance(System.now().date(), System.now().time()));

        Logger.insertLogEntry('Custom Metadata SKEW_LIMIT:' + DS_Execution_Params.SKEW_LIMIT);
        Logger.insertLogEntry('Custom Metadata INCLUDE_CUSTOM_OBJECTS:' + DS_Execution_Params.INCLUDE_CUSTOM_OBJECTS);
        Logger.insertLogEntry('Custom Metadata STANDARD_OBJECTS_TO_CHECK_LIST:' + DS_Execution_Params.STANDARD_OBJECTS_TO_CHECK_LIST);
        Logger.insertLogEntry('Custom Metadata CUSTOM_OBJECT_TO_EXCLUDE_LIST:' + DS_Execution_Params.CUSTOM_OBJECT_TO_EXCLUDE_LIST);
        Logger.insertLogEntry('Custom Metadata DONT_CHECK_FIELDS_LIST:' + DS_Execution_Params.DONT_CHECK_FIELDS_LIST);
        execute(null);
    }

    public void execute(SchedulableContext sc) {

        //If you want to speed up the process keeping the data loaded on previously runs,
        //just set the setting on the metadata type to false
        if (DS_Execution_Params.RELOAD_COUNT_ROW_RELATIONS_OBJECT) {
            getAllObjectsToAnalyze();
            getObjectsRowCount();
        }
    }

    public static void getAllObjectsToAnalyze() {

        //Add all Custom Objects in the ORG, except those excluded explicityly and
        //external objects, events and others weird ones (by externsion)

        for (Schema.SObjectType o : Schema.getGlobalDescribe().values()) {

            String objName = o.getDescribe().getName();
            DescribeSObjectResult objDescribe = o.getDescribe();

            if (objDescribe.isAccessible() && objDescribe.isQueryable()
                    && !objName.endsWith('__e') && !objName.endsWith('__kav') && !objName.endsWith('__x')) {

                if (objDescribe.isCustom()
                        && DS_Execution_Params.INCLUDE_CUSTOM_OBJECTS
                        && !objectInList(objName, DS_Execution_Params.CUSTOM_OBJECT_TO_EXCLUDE_LIST)) {

                    Data_Skew_Object__c customObject = new Data_Skew_Object__c(Name = objName, Is_Custom_Object__c = true);
                    objectsToCheck.add(customObject);

                } else if ( !objDescribe.isCustom() && objectInList(objName, DS_Execution_Params.STANDARD_OBJECTS_TO_CHECK_LIST) ) {
                    Data_Skew_Object__c stdObject = new Data_Skew_Object__c(Name = objName, Is_Custom_Object__c = false);
                    objectsToCheck.add(stdObject);
                }
            }
        }

        try{
            upsert objectsToCheck;
        }catch (Exception e){
            Logger.insertLogEntry('Error: ' + e);
        }finally {
            Logger.insertLogEntry('It will analyze a total of: ' + objectsToCheck.size() + ' objects (standard + custom)');
        }
    }

    public static void getObjectsRowCount() {

        DS_Measure_Objects countRowsObjects = new DS_Measure_Objects();
        Id batchID = Database.executeBatch(countRowsObjects);

        Logger.insertLogEntry('Batch sent for DS_Measure_Objects with ID: ' + batchID);
    }

    public static Boolean objectInList(String objName, List<String> objList){

        Integer count = 0;
        Boolean found = false;
        String objNameNormalized = objName.trim().toUpperCase();
        while ( (count<objList.size() ) && !found){

            String current = objList.get(count).trim().toUpperCase();
            found = objNameNormalized.equals(current);

            count++;
        }

        return found;
    }
}