/**
 * Get fields with data skewed and save results using a Batchable process
 *
 * @author Esteve Graells
 * @date July 2018
 *
 * @description Get fields with data skewed and save results
 * Get full explanation on https://forcegraells.com/2018/07/19/skew-data/
 *
*/


global class DS_Check_Relation_Fields implements Database.Batchable<SObject>, Database.Stateful {

    global DS_Check_Relation_Fields() {
        Logger.insertLogEntry('Start DS_Check_Relation_Fields: ' + Datetime.newInstance(System.now().date(), System.now().time()));
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        //Only 1 record will be returned
        return Database.getQueryLocator(
                        ' SELECT Data_Skew_Object__r.Name, Field_Name__c, Is_Skewed__c, Facts__c' +
                        ' FROM Data_Skew_Field__c');
    }

    global void execute(Database.BatchableContext BC, List<Data_Skew_Field__c> scope) {

        Logger.insertLogEntry('Will analyze: ' + scope.size() + ' fields');

        for (Data_Skew_Field__c o : scope) {

            List<AggregateResult> results = new List<AggregateResult>();

            //Using != operator  will produce a non-selective query for the SOQL optimizer
            //Depending on the volume table this could lead to a slow query performance (watch out!!)
            //Alternatively: delete the condition and discard the null values later on the else statement and create
            //custom indexes on the table (you can delete them afterwards). Check the Query & Search 
            //optimitzation Cheat Sheet for Selectivity threadsholds before creating those custom indexes.
            String q =
                    ' SELECT ' + o.Field_Name__c + ' skewedvalue, ' + 'count(id) counted ' +
                    ' FROM ' + o.Data_Skew_Object__r.Name +
                    ' GROUP BY ' + o.Field_Name__c +
                    ' HAVING count(id) >= ' + DS_Execution_Params.SKEW_LIMIT +
                    ' AND ' + o.Field_Name__c + '!=NULL';

            results = Database.query(q);

            if(results.size()==0){
                o.Is_Skewed__c = false;
                o.Facts__c = 'No Data Skew Found';
                Logger.insertLogEntry('Object: ' + o.Data_Skew_Object__r.Name + ' has NO Skew data on ' + o.Field_Name__c);

            }else{
                o.Is_Skewed__c = true;
                o.Facts__c = 'Data Skew on values: ';
                for (AggregateResult r : results) {
                    o.Facts__c += r.get('skewedvalue') + '(' + r.get('counted') + ' rows) ' ;
            }
                Logger.insertLogEntry('Object: ' + o.Data_Skew_Object__r.Name + ' has Skew Data on: ' + o.Field_Name__c);
            }
        }

        update scope;
    }

    global void finish(Database.BatchableContext BC) {

        Logger.insertLogEntry('Process ended');

    }
}