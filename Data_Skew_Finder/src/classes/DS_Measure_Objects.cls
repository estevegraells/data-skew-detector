/**
 * Count rows for all objects using a Batchable process
 *
 * @author Esteve Graells
 * @date July 2018
 *
 * @description Get the count row for all the collected objects
 * Get full explanation on https://forcegraells.com/2018/07/19/skew-data/
 *
*/


global class DS_Measure_Objects implements Database.Batchable<SObject>, Database.Stateful{

    global DS_Measure_Objects(){
        Logger.insertLogEntry('Start DS_Measure_Objects:  ' + Datetime.newInstance(System.now().date(), System.now().time()));
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Name FROM Data_Skew_Object__c');
    }

    global void execute(Database.BatchableContext BC, List<Data_Skew_Object__c> scope){

        Database.SaveResult[] SR = new Database.SaveResult[]{};
        try{
            for (Data_Skew_Object__c obj : scope) {
                String objName = obj.Name;
                Integer rowCount = Database.countQuery('SELECT count() FROM ' + objName);
                obj.Row_Count__c = rowCount; 
            }

            SR = Database.update(scope);

        }catch (Exception e){
            Logger.insertLogEntry('Error: ' + e);

        }finally {
            Logger.insertLogEntry('Updated records with Record Count: ' + SR.size());
        }
    }

    global void finish(Database.BatchableContext BC){

        //Batch chaining to analysis all field relations from large Objects
        DS_Relations_Object_Finder findObjectRelations = new DS_Relations_Object_Finder();
        Id batchID = Database.executeBatch(findObjectRelations);

        Logger.insertLogEntry('Batch for DS_Relations_Object_Finder sent with ID: ' + batchID);
    }
}