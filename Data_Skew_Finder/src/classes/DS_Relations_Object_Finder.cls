/**
 * Get all relations for Objects using a Batchable process
 *
 * @author Esteve Graells
 * @date July 2018
 *
 * @description Get all field relations for objects collected
 * Get full explanation on https://forcegraells.com/2018/07/19/skew-data/
 *
*/

global class DS_Relations_Object_Finder implements Database.Batchable<SObject>, Database.Stateful {

    public DS_Relations_Object_Finder() {
        Logger.insertLogEntry('Start DS_Relations_Object_Finder: ' +
                Datetime.newInstance(System.now().date(), System.now().time()));
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(
                'SELECT Id, Name, Number_of_Fields_with_relations__c ' +
                'FROM Data_Skew_Object__c ' +
                'WHERE Row_Count__c >= ' + DS_Execution_Params.SKEW_LIMIT);
    }

    global void execute(Database.BatchableContext BC, List<Data_Skew_Object__c> scope) {

        Logger.insertLogEntry('Looking for Data Skew in: ' + scope.size() + ' large objects');

        List<Data_Skew_Field__c> objFieldRelations = new List<Data_Skew_Field__c>();

        for(Data_Skew_Object__c o: scope ){

            //Get fields that are a relation to another object
            List<String> objList = new List<String>();
            objList.add(o.Name);

            Schema.DescribeSObjectResult[] results = Schema.describeSObjects(objList);
            Map<String, SObjectField> fieldList = results[0].fields.getMap();

            Integer relationsFieldCount = 0;
            for (String fieldName : fieldList.keySet()) {

                if ( (fieldName <> null) &&
                        (!DS_Finder.objectInList(fieldName, DS_Execution_Params.DONT_CHECK_FIELDS_LIST)) ) {

                    String relationName = fieldList.get(fieldName).getDescribe().getRelationshipName();

                    if ( !String.isEmpty(relationName) ) {

                        Data_Skew_Field__c objFieldRelation =
                                new Data_Skew_Field__c( Data_Skew_Object__c=o.Id, Field_Name__c = fieldName);
                        objFieldRelations.add(objFieldRelation);
                        relationsFieldCount++;
                    }
                }
            }

            o.Number_of_Fields_with_relations__c = relationsFieldCount;
        }

        //Update Data Skew Object
        upsert scope;

        //Update all object with their relations
        upsert objFieldRelations;
    }


    global void finish(Database.BatchableContext BC) {

        //For any large object with fields launch a Batch to investigate every relation
        DS_Check_Relation_Fields checkAllRelationsObjectBatch = new DS_Check_Relation_Fields();
        Id batchId = Database.executeBatch(checkAllRelationsObjectBatch);
        Logger.insertLogEntry('Batch for DS_Relations_Object_Finder sent with ID: ' + batchId);
    }
}