<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Data Skew Settings Esteve</label>
    <protected>false</protected>
    <values>
        <field>Check_Custom_Objects_for_Data_Skew__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Custom_Object_List_To_avoid_to_check__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Data_Skew_Limit__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Field_List_Not_to_be_checked__c</field>
        <value xsi:type="xsd:string">createdbyid; id; insertedbyid; isdeleted; lastmodifieddate; lastmodifiedbyid; lastactivitydate; lastreferenceddate; lastvieweddate; masterrecordid; recordtypeid; reportstoid; setupownerid; systemmodstamp</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Reload_Count_Row_and_Relations_Table__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Standard_Object_To_Check_List__c</field>
        <value xsi:type="xsd:string">Account; Announcement;  Approval;  Asset; AssetHistory; Attachment; Bookmark; Campaign; CampaignFeed; Case; CaseArticle; CaseComment; CaseFeed; Contact; ContactFeed; Contract; EmailMessage; Event; FeedComment; Folder; Idea; Lead; Note; Opportunity; Order; Partner; Pricebook2; Product2; Question; Quote; Solution; Task; TopicAccount; Announcement;  Approval;  Asset; AssetHistory; Attachment; Bookmark; Campaign; CampaignFeed; Case; CaseArticle; CaseComment; CaseFeed; Contact; ContactFeed; Contract; EmailMessage; Event; FeedComment; Folder; Idea; Lead; Note; Opportunity; Order; Partner; Pricebook2; Product2; Question; Quote; Solution; Task; Topic</value>
    </values>
</CustomMetadata>
